#pragma once
#include "stdafx.h"

#ifdef _DEBUG
#pragma comment(linker, "/entry:WinMainCRTStartup")
#pragma comment(linker, "/subsystem:console")
#endif

// 윈도우 프로시져
LRESULT CALLBACK ChildMsgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK Inspector(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// 인스펙터 함수
VOID SetEdit(Object* obj);

// 진입점
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);

// -----------------------------------------

// Logic
VOID Update(HWND hWnd);

// 어플리케이션 초기화
VOID InitApp(HWND hWnd);

// Key Input
VOID ProcessInput(HWND hWnd, WPARAM keyPress);

// Resize
VOID Resize(INT width, INT height, WPARAM flag);

// Camera Input
VOID CameraInput();

// Middle Mouse Input
VOID MouseWheel(SHORT data);

// RBUTTONDOWN
VOID RButtonInput(MOUSESTATE state, SHORT x_pos, SHORT y_pos, WPARAM flag);