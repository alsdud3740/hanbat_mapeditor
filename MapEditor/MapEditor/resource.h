//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MapEditor.rc에서 사용되고 있습니다.
//
#define IDD_CREATELOG                   9
#define IDD_CUBEDIALOG                  101
#define IDR_MainMenu                    104
#define IDS_APP_TITLE                   107
#define IDC_NEWPROJ                     108
#define IDD_IP                          108
#define ID_TEXTUREPIC                   1001
#define IDC_POSX                        1003
#define IDC_POSY                        1004
#define IDC_POSZ                        1005
#define IDC_ROTX                        1006
#define IDC_IPOSX                       1006
#define IDC_ROTY                        1007
#define IDC_ISX                         1007
#define IDC_ROTZ                        1008
#define IDC_ISY                         1008
#define IDC_SCALEX                      1009
#define IDC_ISZ                         1009
#define IDC_SCALEY                      1010
#define IDC_IRX                         1010
#define IDC_SCALEZ                      1011
#define IDC_IRY                         1011
#define IDC_EDIT9                       1012
#define IDC_IRZ                         1012
#define IDC_IPOSY                       1013
#define IDC_IPOSZ                       1014
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_40003                        40003
#define ID_SAVE                         40004
#define ID_LOAD                         40005
#define ID_CAMINIT                      40006
#define ID_40007                        40007
#define ID_                             40008
#define ID_Menu                         40009
#define ID_ADD                          40010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40011
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
