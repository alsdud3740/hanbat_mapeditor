#include "stdafx.h"
#include "System.h"

extern HDC g_hdc;
extern UINT Mouse_X;
extern UINT Mouse_Y;
extern HWND ChildHwnd[2];

UINT index;

UINT CheckRGB()
{
	glFlush();
	glFinish();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	RECT rect;
	GetClientRect(ChildHwnd[0], &rect);

	unsigned char data[4];
	glReadPixels(Mouse_X, rect.bottom - Mouse_Y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);

	//printf("Mouse X - %d, Mosue Y - %d, CONVER MOSUE - %d %d, RGBA - %d %d %d %d\n", Mouse_X, Mouse_Y, Mouse_X, rect.bottom - Mouse_Y, data[0], data[1], data[2], data[3]);

	return data[0] + data[1] * 256 + data[2] * 256 * 256;
}

VOID Update(HWND hWnd)
{
	// TODO:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	OBJECTMANAGER->DrawForPick();
	index = CheckRGB();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	MAP->Draw();

	SwapBuffers(g_hdc);
}