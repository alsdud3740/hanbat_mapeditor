#pragma once

#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <array>
#include <list>
#include <chrono>
#include <ctime>
#include <functional>
#include <iostream>
#include <vector>
#include <memory>
#include <unordered_map>
#include <sstream>
#include <fstream>


// ��ũ��
#define snprintf(dst, format, ...)     _snprintf_s(dst.data(), dst.size(), _TRUNCATE, format, __VA_ARGS__)
#define snwprintf(dst, format, ...)    _snwprintf_s(dst.data(), dst.size(), _TRUNCATE, format, __VA_ARGS__)

// Resource
#include "resource.h"

// Image Util
#include <SOIL.h>

// OpenGL
#define GLEW_STATIC
#include <gl\glew.h>
#include <gl\glut.h>
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Util
#include "Util\Type.h"
#include "Util\Singleton.h"
#include "Util\LowFragmentationHeap.h"
#include "Util\MemoryPool.h"
#include "Util\Clock.h"
#include "Util\Minidumb.h"

// Object
#include "Object\Shader.h"
#include "Object\Vertex.h"
#include "Object\Texture.h"
#include "Object\Mesh.h"
#include "Object\Object.h"
#include "Object\Camera.h"
#include "Object\DirectionalLight.h"

// Object Manager
#include "Object\TextureManager.h"
#include "Object\MeshManager.h"
#include "Object\ShaderManager.h"
#include "Object\ObjectManager.h"

// Map
#include "Map\MapManager.h"

// MainFrame
#include "System.h"