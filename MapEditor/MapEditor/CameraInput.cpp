#include "stdafx.h"
#include "System.h"

VOID CameraInput()
{
	if (GetAsyncKeyState(0x44) == 0xffff8000)
	{
		Camera::getInstance()->ProcessKeyboard(RIGHT, 0.1f);
	}

	if (GetAsyncKeyState(0x57) == 0xffff8000)
	{
		Camera::getInstance()->ProcessKeyboard(UP, 0.1f);
	}

	if (GetAsyncKeyState(0x41) == 0xffff8000)
	{
		Camera::getInstance()->ProcessKeyboard(LEFT, 0.1f);
	}

	if (GetAsyncKeyState(0x53) == 0xffff8000)
	{
		Camera::getInstance()->ProcessKeyboard(DOWN, 0.1f);
	}
}