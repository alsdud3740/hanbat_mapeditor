#pragma once
#include "..\stdafx.h"
#include "..\Util\MemoryPool.h"

class Object : public MemoryPool<Object>
{
public:
	glm::vec3 transform;
	glm::vec3 scale;
	glm::vec3 rotate;
	Mesh mesh;
	UINT index;

	// ������
	Object() {}

	Object(glm::vec3 transform, glm::vec3 scale, glm::vec3 rotate, Mesh mesh)
	{
		this->transform = transform;
		this->scale = scale;
		this->rotate = rotate;
		this->mesh = mesh;
	}
};