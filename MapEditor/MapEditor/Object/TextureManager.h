#pragma once
#include "..\stdafx.h"

#define TEXTUREMANAGER TextureManager::getInstance()

class TextureManager : public Singleton<TextureManager>
{
public:
	GLuint getTexture(std::string path)
	{
		printf("getTexture:%s\n", path.c_str());

		auto iter = this->textures.find(path);

		if (iter != this->textures.end())
		{
			return iter->second.id;
		}
		else
		{
			return GenTexture(path);
		}
	}

private:
	GLuint GenTexture(std::string path)
	{
		printf("GenTexture:%s\n", path.c_str());
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		int width, height;
		unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		SOIL_free_image_data(image);
		glBindTexture(GL_TEXTURE_2D, 0);

		Texture* temp = new Texture;
		temp->id = texture;
		temp->path = path;

		this->textures.insert(std::unordered_map<std::string, Texture>::value_type(path, *temp));

		return texture;
	}

	std::unordered_map<std::string, Texture> textures;
};