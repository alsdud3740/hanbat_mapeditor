#include "..\stdafx.h"
#include "Mesh.h"

Mesh::Mesh(vector<Vertex> vertices, vector<GLuint> indices, std::string texturePath)
{
	this->vertices = vertices;
	this->indices = indices;
	
	if (this->texture.path != "NULL")
	{
		this->texture.path = texturePath;
		this->texture.id = TEXTUREMANAGER->getTexture(texturePath);
	}

	this->setupMesh();
	this->shaderNum = 0;
	this->SetShader();
}

VOID Mesh::setupMesh()
{
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex),
		&this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
		&this->indices[0], GL_STATIC_DRAW);

	// Vertex Positions
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// Vertex Normal
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 
		(GLvoid*)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(1);

	// Vertex Texture Corrds
	if (!this->texture.path.empty()) {
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			(GLvoid*)offsetof(Vertex, TexCoords));
		glEnableVertexAttribArray(2);
	}

	// Vertex Color
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Color));
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);
}

VOID Mesh::Draw()
{
	if (this->texture.path != "NULL")
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->texture.id);
		glUniform1i(glGetUniformLocation(shader.Program, "ourTexture1"), 0);
	}
	glBindVertexArray(this->VAO);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

VOID Mesh::SetShader()
{
	this->shader = *SHADERMANAGER->getShader(this->shaderNum);
}