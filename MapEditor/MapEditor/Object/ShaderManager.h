#pragma once
#include "..\stdafx.h"

#define SHADERMANAGER ShaderManager::getInstance()

class ShaderManager : public Singleton<ShaderManager>
{
private:
	std::vector<Shader*> shaders;
	INT index;

public:
	// ������
	ShaderManager()
	{
		index = 0;
		CreateBasicShader();
	}

	VOID CreateBasicShader()
	{
		std::cout << "CreateBasicShader" << std::endl;
		Shader* shader = new Shader("Shader\\Basic.vs", "Shader\\Basic.frag");
		this->shaders.push_back(shader);
		index++;

		std::cout << "CreatePickShader" << std::endl;
		Shader* shader2 = new Shader("Shader\\Pick.vs", "Shader\\Pick.frag");
		this->shaders.push_back(shader2);
		index++;
	}

	VOID CreateShader(const char* vs_path, const char* fr_path)
	{
		Shader* shader = new Shader(vs_path, fr_path);
		this->shaders.push_back(shader);
		index++;
	}

	Shader* getShader(GLuint num)
	{
		return this->shaders[num];
	}
};
