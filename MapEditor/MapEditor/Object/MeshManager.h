#pragma once
#include "..\stdafx.h"

#define MESHMANAGER MeshManager::getInstance()

class MeshManager : public Singleton<MeshManager>
{
public:
	Mesh* getMesh(std::string name)
	{
		printf("getMesh:%s\n", name.c_str());

		auto iter = this->meshes.find(name);

		if (iter != this->meshes.end())
		{
			return iter->second;
		}
		else
		{
			printf("NOT_FOUNDED_MESH : %s\n", name.c_str());
		}
	}

	BOOL storeMesh(std::string name, Mesh* mesh, GLuint shaderNum)
	{
		printf("storeMesh : %s\n", name.c_str());

		Mesh* newMesh = new Mesh(mesh->vertices, mesh->indices, mesh->texture.path);

		if (newMesh == nullptr)
			return FALSE;

		newMesh->shaderNum = shaderNum;
		newMesh->SetShader();

		this->meshes.insert(std::unordered_map<std::string, Mesh*>::value_type(name, newMesh));

		return TRUE;
	}

private:
	std::unordered_map<std::string, Mesh*> meshes;
};