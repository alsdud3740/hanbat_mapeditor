#pragma once
#include "..\stdafx.h"

class DirectionalLight : public Singleton<DirectionalLight>
{
private:
	glm::vec3 direction;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

public:
	// ������
	DirectionalLight(glm::vec3 direction = glm::vec3(-0.2f, -1.0f, -0.3f), glm::vec3 ambient = glm::vec3(0.05f, 0.05f, 0.05f),
		glm::vec3 diffuse = glm::vec3(0.4f, 0.4f, 0.4f), glm::vec3 specular = glm::vec3(0.5f, 0.5f, 0.5f))
	{
		this->direction = glm::vec3(-0.2f, -1.0f, -0.3f);
		this->ambient = glm::vec3(0.05f, 0.05f, 0.05f);
		this->diffuse = glm::vec3(0.4f, 0.4f, 0.4f);
		this->specular = glm::vec3(0.5f, 0.5f, 0.5f);
	}

	VOID getUniform(Shader shader)
	{
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "viewPos"), 1, GL_FALSE, glm::value_ptr(Camera::getInstance()->Position));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "dirLight.direction"), 1, GL_FALSE, glm::value_ptr(this->direction));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "dirLight.ambient"), 1, GL_FALSE, glm::value_ptr(this->ambient));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "dirLight.diffuse"), 1, GL_FALSE, glm::value_ptr(this->direction));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "dirLight.specular"), 1, GL_FALSE, glm::value_ptr(this->specular));
	}
};