#pragma once
#include "..\stdafx.h"

class Mesh {
public:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	Texture texture;
	GLuint shaderNum;
	Shader shader;
	
	// 생성자
	Mesh(vector<Vertex> vertices, vector<GLuint> indices, std::string texturePath = "NULL");
	Mesh() {}

	// 함수
	VOID Draw();
	VOID SetShader();
	
public:
	GLuint VAO, VBO, EBO;
	VOID setupMesh();
};