#pragma once
#include "..\stdafx.h"

struct Texture {
	GLuint id;
	std::string path;
};