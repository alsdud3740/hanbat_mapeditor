#pragma once
#include "..\stdafx.h"

#define OBJECTMANAGER ObjectManager::getInstance()

class ObjectManager : public Singleton<ObjectManager>
{
public:
	ObjectManager()
	{
		this->index = 0;
	}

	VOID Draw()
	{
		for (auto i : this->objects)
		{
			i->mesh.shader.Use();

			DirectionalLight::getInstance()->getUniform(i->mesh.shader);

			glm::mat4 view;
			glm::mat4 projection;
			glm::mat4 model;
			
			view = Camera::getInstance()->GetViewMatrix();
			projection = glm::perspective(45.0f, (GLfloat)WIN_WIDTH / (GLfloat)WIN_HEIGHT, 0.1f, 100.0f);
			model = glm::translate(model, i->transform);
			model = glm::rotate(model, i->rotate.x, glm::vec3(1.0f, 0.0f, 0.0f));
			model = glm::rotate(model, i->rotate.y, glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::rotate(model, i->rotate.z, glm::vec3(0.0f, 0.0f, 1.0f));
			model = glm::scale(model, i->scale);

			GLint modelLoc = glGetUniformLocation(i->mesh.shader.Program, "model");
			GLint viewLoc = glGetUniformLocation(i->mesh.shader.Program, "view");
			GLint projLoc = glGetUniformLocation(i->mesh.shader.Program, "projection");
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
			
			i->mesh.Draw();

			glUseProgram(0);
		}
	}

	glm::vec3 getColorWithIndex(UINT index)
	{
		int r = (index & 0x000000FF) >> 0;
		int g = (index & 0x0000FF00) >> 8;
		int b = (index & 0x00FF0000) >> 16;

		return glm::vec3(r, g, b);
	}

	VOID DrawForPick()
	{
		for (auto& i : this->objects)
		{
			SHADERMANAGER->getShader(1)->Use();

			glm::mat4 view;
			glm::mat4 projection;
			glm::mat4 model;

			view = Camera::getInstance()->GetViewMatrix();
			projection = glm::perspective(45.0f, (GLfloat)WIN_WIDTH / (GLfloat)WIN_HEIGHT, 0.1f, 100.0f);
			model = glm::translate(model, i->transform);
			model = glm::rotate(model, i->rotate.x, glm::vec3(1.0f, 0.0f, 0.0f));
			model = glm::rotate(model, i->rotate.y, glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::rotate(model, i->rotate.z, glm::vec3(0.0f, 0.0f, 1.0f));
			model = glm::scale(model, i->scale);

			GLint modelLoc = glGetUniformLocation(SHADERMANAGER->getShader(1)->Program, "model");
			GLint viewLoc = glGetUniformLocation(SHADERMANAGER->getShader(1)->Program, "view");
			GLint projLoc = glGetUniformLocation(SHADERMANAGER->getShader(1)->Program, "projection");
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

			glUniform4f(glGetUniformLocation(SHADERMANAGER->getShader(1)->Program, "Colors"), getColorWithIndex(i->index).x/255.0f, getColorWithIndex(i->index).y/255.0f, getColorWithIndex(i->index).z/255.0f, 1.0f);

			i->mesh.Draw();

			glUseProgram(0);
		}
	}

	BOOL StoreOBJ(Object* obj)
	{
		printf("StoreOBJ\n");
		Object* newOBJ = new Object(obj->transform, obj->scale, obj->rotate, obj->mesh);
		Object* newOBJ2 = new Object(obj->transform, obj->scale, obj->rotate, obj->mesh);

		if (newOBJ == nullptr || newOBJ2 == nullptr)
			return FALSE;

		newOBJ->index = this->index;
		index++;
		this->objects.push_back(newOBJ);

		return TRUE;
	}

	Object* FindObjWithIndex(UINT index)
	{
		printf("FindObjWithIndex = %d\n", index);

		for (auto* i : this->objects)
		{
			if (i->index == index)
			{
				return i;
			}
		}

		return nullptr;
	}

	std::vector<Object*> objects;
	UINT index;
};