#include "stdafx.h"
#include "System.h"

VOID RButtonInput(MOUSESTATE state, SHORT x_pos, SHORT y_pos, WPARAM flag)
{
	static BOOL isFirst = true;
	static SHORT lastX;
	static SHORT lastY;

	if (state == MOUSESTATE::D)
	{
		if (isFirst) {
			lastX = x_pos;
			lastY = y_pos;
			isFirst = false;
		}
	}
	else if (state == MOUSESTATE::U)
	{
		isFirst = true;
	}
	else if (state == MOUSESTATE::M)
	{
		if (!isFirst) {
			SHORT x = lastX - x_pos;
			SHORT y = lastY - y_pos;
			Camera::getInstance()->ProcessMouseMovement(-x, y);

			lastX = x_pos;
			lastY = y_pos;
		}
	}
}