#include "..\stdafx.h"
#include "MapManager.h"

VOID MapManager::LoadMap(HWND hWnd)
{
	OPENFILENAME ofn;
	TCHAR strFile[MAX_PATH] = L"";

	memset(&ofn, 0, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME); 
	ofn.hwndOwner = hWnd;     
	ofn.Flags = OFN_NOCHANGEDIR | OFN_PATHMUSTEXIST;
	ofn.lpstrFilter = L"Map File(*.*)\0*.*;\0";
	ofn.lpstrFile = strFile;
	ofn.nMaxFile = MAX_PATH;
	if (GetOpenFileName(&ofn) != 0)
	{
		ifstream fileIn;

		fileIn.open(strFile, ios::binary | ios::in);

		if (!fileIn.is_open())
			return;

		while (!fileIn.eof())
		{
			glm::vec3 pos;
			glm::vec3 rot;
			glm::vec3 scale;
			CHAR path[128];

			fileIn.read((CHAR*)&pos.x, sizeof(FLOAT));
			fileIn.read((CHAR*)&pos.y, sizeof(FLOAT));
			fileIn.read((CHAR*)&pos.z, sizeof(FLOAT));

			fileIn.read((CHAR*)&rot.x, sizeof(FLOAT));
			fileIn.read((CHAR*)&rot.y, sizeof(FLOAT));
			fileIn.read((CHAR*)&rot.z, sizeof(FLOAT));

			fileIn.read((CHAR*)&scale.x, sizeof(FLOAT));
			fileIn.read((CHAR*)&scale.y, sizeof(FLOAT));
			fileIn.read((CHAR*)&scale.z, sizeof(FLOAT));

			INT i = 0;

			while (TRUE)
			{
				fileIn.read(path + i, sizeof(CHAR));

				if (path[i++] == 0)
					break;
			}

			Mesh mesh = *MESHMANAGER->getMesh("CUBE");
			mesh.texture.path = path;
			Object obj(pos, scale, rot, mesh);
			OBJECTMANAGER->StoreOBJ(&obj);
		}

		fileIn.close();
	}
}

VOID MapManager::SaveMap(HWND hWnd)
{
	OPENFILENAME ofn;
	TCHAR strFile[MAX_PATH] = L"";

	memset(&ofn, 0, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.Flags = OFN_NOCHANGEDIR | OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT;
	ofn.lpstrFilter = L"Map File(*.*)\0*.*;\0";
	ofn.lpstrFile = strFile;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrTitle = L"����";
	if (GetOpenFileName(&ofn) != 0)
	{
		ofstream fileOut;
		fileOut.open(strFile, ios::binary | ios::out);
		
		if (!fileOut.is_open())
			return;

		for (auto& i : OBJECTMANAGER->objects)
		{
			fileOut.write((CHAR*)&i->transform.x, sizeof(FLOAT));
			fileOut.write((CHAR*)&i->transform.y, sizeof(FLOAT));
			fileOut.write((CHAR*)&i->transform.z, sizeof(FLOAT));

			fileOut.write((CHAR*)&i->rotate.x, sizeof(FLOAT));
			fileOut.write((CHAR*)&i->rotate.y, sizeof(FLOAT));
			fileOut.write((CHAR*)&i->rotate.z, sizeof(FLOAT));

			fileOut.write((CHAR*)&i->scale.x, sizeof(FLOAT));
			fileOut.write((CHAR*)&i->scale.y, sizeof(FLOAT));
			fileOut.write((CHAR*)&i->scale.z, sizeof(FLOAT));

			fileOut << i->mesh.texture.path.c_str();
			CHAR temp = 0;
			fileOut.write(&temp, 1);
		}

		fileOut.close();
	}
}