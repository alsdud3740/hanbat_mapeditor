#pragma once
#include "..\stdafx.h"

#define MAP MapManager::getInstance()

class MapManager : public Singleton<MapManager>
{
public:
	VOID LoadMap(HWND hWnd);
	VOID SaveMap(HWND hWnd);
	VOID Draw() {
		OBJECTMANAGER->Draw();
	}
};