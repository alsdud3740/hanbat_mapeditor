#include "stdafx.h"
#include "System.h"
#include "Util\Singleton.h"
#include "Object\MeshManager.h"

HDC g_hdc;
HGLRC g_hRc;
extern HINSTANCE gInst;
GLuint VBO;

// Function
VOID CreateChild(HWND hWnd);
VOID CreateContext(HWND hWnd);
VOID InitGLEW();
VOID InitGLMode();
VOID InitPrimitiveMesh();

// Main Function
VOID InitApp(HWND hWnd)
{
	// TODO:
	CreateContext(hWnd);
	InitGLEW();
	InitGLMode();
	InitPrimitiveMesh();
}

// ---------------------------------------------------
VOID CreateContext(HWND hWnd)
{
	int nPixelFormat;
	g_hdc = GetDC(hWnd);

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		GetDeviceCaps(g_hdc, BITSPIXEL),
		0,0,0,0,0,0,
		0,0,0,0,0,0,0,
		32,
		0,0,
		PFD_MAIN_PLANE,
		0,
		0,0,0
	};

	nPixelFormat = ChoosePixelFormat(g_hdc, &pfd);

	if (nPixelFormat == 0) {
		fprintf(stderr, "ChoosePixelFormat() Error");
		exit(1);
	}

	if (!SetPixelFormat(g_hdc, nPixelFormat, &pfd))
	{
		fprintf(stderr, "SetPixelFormat() Error");
		exit(1);
	}

	g_hRc = wglCreateContext(g_hdc);

	if (g_hRc == nullptr)
	{
		fprintf(stderr, "wglCreateContext() Error");
		exit(1);
	}

	if (!wglMakeCurrent(g_hdc, g_hRc))
	{
		fprintf(stderr, "wglMakeCurrent() ERror");
		exit(1);
	}
}
VOID InitGLEW()
{
	glewExperimental = TRUE;
	
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "glewInit() Error");
		exit(1);
	}
}
VOID InitGLMode()
{
	glClearColor(0.3f, 0.7f, 0.8f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_BACK, GL_LINE);
}
VOID InitPrimitiveMesh()
{
	// CUBE
#pragma region
	std::vector<Vertex> vertecies;

	// �ո�
	Vertex ver0;
	ver0.Position = glm::vec3(-0.5, -0.5, 0.0);
	ver0.TexCoords = glm::vec2(0.0, 0.0);
	ver0.Normal = glm::vec3(0.0f, 0.0f, 1.0f);
	Vertex ver1;
	ver1.Position = glm::vec3(0.5, -0.5, 0.0);
	ver1.TexCoords = glm::vec2(1.0, 0.0);
	ver1.Normal = glm::vec3(0.0f, 0.0f, 1.0f);
	Vertex ver2;
	ver2.Position = glm::vec3(0.5, 0.5, 0.0);
	ver2.TexCoords = glm::vec2(1.0, 1.0);
	ver2.Normal = glm::vec3(0.0f, 0.0f, 1.0f);
	Vertex ver3;
	ver3.Position = glm::vec3(-0.5, 0.5, 0.0);
	ver3.TexCoords = glm::vec2(0.0, 1.0);
	ver3.Normal = glm::vec3(0.0f, 0.0f, 1.0f);

	vertecies.push_back(ver0);
	vertecies.push_back(ver1);
	vertecies.push_back(ver2);
	vertecies.push_back(ver3);

	// ������ ����
	Vertex ver4;
	ver4.Position = glm::vec3(0.5, -0.5, 0.0);
	ver4.TexCoords = glm::vec2(0.0, 0.0);
	ver4.Normal = glm::vec3(1.0f, 0.0f, 0.0f);
	Vertex ver5;
	ver5.Position = glm::vec3(0.5, -0.5, -1.0);
	ver5.TexCoords = glm::vec2(1.0, 0.0);
	ver5.Normal = glm::vec3(1.0f, 0.0f, 0.0f);
	Vertex ver6;
	ver6.Position = glm::vec3(0.5, 0.5, -1.0);
	ver6.TexCoords = glm::vec2(1.0, 1.0);
	ver6.Normal = glm::vec3(1.0f, 0.0f, 0.0f);
	Vertex ver7;
	ver7.Position = glm::vec3(0.5, 0.5, 0.0);
	ver7.TexCoords = glm::vec2(0.0, 1.0);
	ver7.Normal = glm::vec3(1.0f, 0.0f, 0.0f);

	vertecies.push_back(ver4);
	vertecies.push_back(ver5);
	vertecies.push_back(ver6);
	vertecies.push_back(ver7);

	// �޸�
	Vertex ver8;
	ver8.Position = glm::vec3(0.5, 0.5, -1.0);
	ver8.TexCoords = glm::vec2(0.0, 1.0);
	ver8.Normal = glm::vec3(0.0f, 0.0f, -1.0f);
	Vertex ver9;
	ver9.Position = glm::vec3(0.5, -0.5, -1.0);
	ver9.TexCoords = glm::vec2(0.0, 0.0);
	ver9.Normal = glm::vec3(0.0f, 0.0f, -1.0f);
	Vertex ver10;
	ver10.Position = glm::vec3(-0.5, -0.5, -1.0);
	ver10.TexCoords = glm::vec2(1.0, 0.0);
	ver10.Normal = glm::vec3(0.0f, 0.0f, -1.0f);
	Vertex ver11;
	ver11.Position = glm::vec3(-0.5, 0.5, -1.0);
	ver11.TexCoords = glm::vec2(1.0, 1.0);
	ver11.Normal = glm::vec3(0.0f, 0.0f, -1.0f);

	vertecies.push_back(ver8);
	vertecies.push_back(ver9);
	vertecies.push_back(ver10);
	vertecies.push_back(ver11);

	// ���� ����
	Vertex ver12;
	ver12.Position = glm::vec3(-0.5, -0.5, 0.0);
	ver12.TexCoords = glm::vec2(1.0, 0.0);
	ver12.Normal = glm::vec3(-1.0f, 0.0f, 0.0f);
	Vertex ver13;
	ver13.Position = glm::vec3(-0.5, -0.5, -1.0);
	ver13.TexCoords = glm::vec2(0.0, 0.0);
	ver13.Normal = glm::vec3(-1.0f, 0.0f, 0.0f);
	Vertex ver14;
	ver14.Position = glm::vec3(-0.5, 0.5, -1.0);
	ver14.TexCoords = glm::vec2(0.0, 1.0);
	ver14.Normal = glm::vec3(-1.0f, 0.0f, 0.0f);
	Vertex ver15;
	ver15.Position = glm::vec3(-0.5, 0.5, 0.0);
	ver15.TexCoords = glm::vec2(1.0, 1.0);
	ver15.Normal = glm::vec3(-1.0f, 0.0f, 0.0f);

	vertecies.push_back(ver12);
	vertecies.push_back(ver13);
	vertecies.push_back(ver14);
	vertecies.push_back(ver15);

	// ����
	Vertex ver16;
	ver16.Position = glm::vec3(-0.5, 0.5, 0.0);
	ver16.TexCoords = glm::vec2(0.0, 0.0);
	ver16.Normal = glm::vec3(0.0f, 1.0f, 0.0f);
	Vertex ver17;
	ver17.Position = glm::vec3(0.5, 0.5, 0.0);
	ver17.TexCoords = glm::vec2(1.0, 0.0);
	ver17.Normal = glm::vec3(0.0f, 1.0f, 0.0f);
	Vertex ver18;
	ver18.Position = glm::vec3(0.5, 0.5, -1.0);
	ver18.TexCoords = glm::vec2(1.0, 1.0);
	ver18.Normal = glm::vec3(0.0f, 1.0f, 0.0f);
	Vertex ver19;
	ver19.Position = glm::vec3(-0.5, 0.5, -1.0);
	ver19.TexCoords = glm::vec2(0.0, 1.0);
	ver19.Normal = glm::vec3(0.0f, 1.0f, 0.0f);

	vertecies.push_back(ver16);
	vertecies.push_back(ver17);
	vertecies.push_back(ver18);
	vertecies.push_back(ver19);

	// �ظ�
	Vertex ver20;
	ver20.Position = glm::vec3(-0.5, -0.5, 0.0);
	ver20.TexCoords = glm::vec2(0.0, 0.0);
	ver20.Normal = glm::vec3(0.0f, -1.0f, 0.0f);
	Vertex ver21;
	ver21.Position = glm::vec3(-0.5, -0.5, -1.0);
	ver21.TexCoords = glm::vec2(0.0, 1.0);
	ver21.Normal = glm::vec3(0.0f, -1.0f, 0.0f);
	Vertex ver22;
	ver22.Position = glm::vec3(0.5, -0.5, -1.0);
	ver22.TexCoords = glm::vec2(1.0, 1.0);
	ver22.Normal = glm::vec3(0.0f, -1.0f, 0.0f);
	Vertex ver23;
	ver23.Position = glm::vec3(0.5, -0.5, 0.0);
	ver23.TexCoords = glm::vec2(1.0, 0.0);
	ver23.Normal = glm::vec3(0.0f, -1.0f, 0.0f);

	vertecies.push_back(ver20);
	vertecies.push_back(ver21);
	vertecies.push_back(ver22);
	vertecies.push_back(ver23);


	std::vector<GLuint> indices;
	// �ո�
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(2);
	indices.push_back(2);
	indices.push_back(3);
	indices.push_back(0);
	// ������ ����
	indices.push_back(4);
	indices.push_back(5);
	indices.push_back(6);
	indices.push_back(6);
	indices.push_back(7);
	indices.push_back(4);
	// �޸�
	indices.push_back(10);
	indices.push_back(11);
	indices.push_back(8);
	indices.push_back(8);
	indices.push_back(9);
	indices.push_back(10);
	// ���� ����
	indices.push_back(12);
	indices.push_back(15);
	indices.push_back(14);
	indices.push_back(14);
	indices.push_back(13);
	indices.push_back(12);
	// ����
	indices.push_back(16);
	indices.push_back(17);
	indices.push_back(18);
	indices.push_back(18);
	indices.push_back(19);
	indices.push_back(16);
	// �ظ�
	indices.push_back(20);
	indices.push_back(21);
	indices.push_back(22);
	indices.push_back(22);
	indices.push_back(23);
	indices.push_back(20);

	Mesh mesh(vertecies, indices, "Texture\\wall.jpg");
	MESHMANAGER->storeMesh("CUBE", &mesh, 0);
#pragma endregion
}