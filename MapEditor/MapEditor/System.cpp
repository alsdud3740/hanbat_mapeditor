#include "stdafx.h"
#include "System.h"

// 전역 변수:
HINSTANCE hInst;                              
WCHAR szTitle[LOADMAXSTRING] = TEXT("MapEditor");            
WCHAR szWindowClass[LOADMAXSTRING] = TEXT("MapEditor");
HINSTANCE gInst;
HWND ChildHwnd[2];

VOID ADD(HWND hDlg)
{
	Object obj(glm::vec3(GetDlgItemInt(hDlg, IDC_POSX, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_POSY, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_POSZ, NULL, TRUE)),
		glm::vec3(GetDlgItemInt(hDlg, IDC_SCALEX, NULL, TRUE),
			GetDlgItemInt(hDlg, IDC_SCALEX, NULL, TRUE),
			GetDlgItemInt(hDlg, IDC_SCALEX, NULL, TRUE)),
		glm::vec3(GetDlgItemInt(hDlg, IDC_ROTX, NULL, TRUE),
			GetDlgItemInt(hDlg, IDC_ROTY, NULL, TRUE),
			GetDlgItemInt(hDlg, IDC_ROTY, NULL, TRUE)), *MESHMANAGER->getMesh("CUBE"));

	printf("ADD pos %d %d %d rot %d %d %d scale %d %d %d\n", GetDlgItemInt(hDlg, IDC_POSX, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_POSY, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_POSZ, NULL, TRUE), GetDlgItemInt(hDlg, IDC_ROTX, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_ROTY, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_ROTY, NULL, TRUE),GetDlgItemInt(hDlg, IDC_SCALEX, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_SCALEY, NULL, TRUE),
		GetDlgItemInt(hDlg, IDC_SCALEZ, NULL, TRUE));

	OBJECTMANAGER->StoreOBJ(&obj);
}

BOOL CALLBACK CubeDlgProc(HWND hDlg, UINT iMessge, WPARAM wParam, LPARAM lParam)
{
	switch (iMessge)
	{
	case WM_INITDIALOG:
		SetDlgItemInt(hDlg, IDC_POSX, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_POSY, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_POSZ, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_ROTX, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_ROTY, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_ROTZ, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_SCALEX, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_SCALEY, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_SCALEZ, 0, TRUE);
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			ADD(hDlg);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, 0);
			return TRUE;
		}
		break;
	}

	return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 여기에 코드를 입력합니다.
	gInst = hInstance;
	MyRegisterClass(hInstance);

	// 응용 프로그램 초기화를 수행합니다.
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NEWPROJ));

	MSG msg;

	// 기본 메시지 루프입니다.
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		CameraInput();

		if (!IsDialogMessage(ChildHwnd[1], &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
	}

	return (int)msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDC_ICON);
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDR_MainMenu);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, IDC_ICON);

	RegisterClassExW(&wcex);

	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 3);
	wcex.lpfnWndProc = ChildMsgProc;
	wcex.lpszClassName = TEXT("ChildLeft");

	return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_CAPTION | WS_SYSMENU,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		ChildHwnd[0] = CreateWindow(TEXT("ChildLeft"), NULL, WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN, 0, 0, 0, 0, hWnd, NULL, gInst, NULL);
		ChildHwnd[1] = CreateDialog(gInst, (LPCTSTR)IDD_IP, hWnd, (DLGPROC)(WNDPROC)Inspector);
		break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 메뉴 선택을 구문 분석합니다.
		switch (wmId)
		{
		case ID_SAVE:
			MAP->SaveMap(hWnd);
			break;
		case ID_LOAD:
			MAP->LoadMap(hWnd);
			break;
		case ID_CAMINIT:
			Camera::getInstance()->Initialization();
			break;
		case ID_ADD:
			DialogBox(gInst, MAKEINTRESOURCE(IDD_CREATELOG), hWnd, (DLGPROC)CubeDlgProc);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_MOUSEWHEEL:
		MouseWheel((SHORT)HIWORD(wParam));
		break;
	case WM_SIZE:
		if (wParam != SIZE_MINIMIZED)
		{
			RECT rect;

			GetClientRect(hWnd, &rect);
			MoveWindow(ChildHwnd[0], 0, 0, rect.right - 200, rect.bottom, TRUE);
			MoveWindow(ChildHwnd[1], rect.right - 200, 0, 200, rect.bottom, TRUE);
		}
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}