#pragma once
#include "..\stdafx.h"
#include <list>

template <class T, INT ALLOC_BLOCK_SIZE = 50>
class MemoryPool
{
protected:
	~MemoryPool(){}

private:
	static std::list<T*> freePointer;
	
	// 새로 할당하는 함수
	static VOID allockBlock()
	{
		T* newPointer = new T[ALLOC_BLOCK_SIZE];

		for (INT i = 0; i < ALLOC_BLOCK_SIZE - 1; i++)
		{
			freePointer.push_back(newPointer + i);
		}
	}

public:
	// new 연산을 할 경우 호출된다.
	static VOID* operator new(std::size_t allockLength)
	{
		// 남은 객체가 없을 경우 새로 할당한다.
		if (freePointer.empty())
			allockBlock();

		auto iter = freePointer.begin();
		T* returnPointer = *iter;
		freePointer.pop_front();

		return returnPointer;
	}

	// delete 연산자를 했을 경우 호출된다.
	static VOID operator delete(VOID* deletePointer)
	{
		T* newPointer = reinterpret_cast<T*>(deletePointer);

		freePointer.push_back(newPointer);
	}
};

template <class T, int ALLOC_BLOCK_SIZE = 50>
std::list<T*> MemoryPool<T, ALLOC_BLOCK_SIZE>::freePointer;