#pragma once
#include "..\stdafx.h"

class LowFragmentationHeap
{
public:
	LowFragmentationHeap()
	{
		static BOOL init = false;
		if (init)
			return;

		init = true;

		ULONG HeapFragValue = 2;
		HANDLE hHeaps[100];
		DWORD dwHeapCount = GetProcessHeaps(100, hHeaps);

		for (DWORD i = 0; i < dwHeapCount; i++)
		{
			HeapSetInformation(hHeaps[i], HeapCompatibilityInformation, &HeapFragValue, sizeof(HeapFragValue));
		}
	}
};

static LowFragmentationHeap lfh;