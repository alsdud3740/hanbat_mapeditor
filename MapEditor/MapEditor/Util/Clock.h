#pragma once
#include "..\stdafx.h"
#include <chrono>
#include <ctime>

using namespace std::chrono;
using namespace std;

typedef enum : int {
	DAY_SUNDAY = 0,
	DAY_MONDAY = 1,
	DAY_TUESDAY = 2,
	DAY_WEDNESDAY = 3,
	DAY_THURSDAY = 4,
	DAY_FRIDAY = 5,
	DAY_SATURDAY = 6,
}DayOfTheWeek;

#define DATETIME_FORMAT         L"D%Y-%m-%dT%H:%M:%S"
#define DATE_FORMAT             L"%Y-%m-%d"
#define TIME_FORMAT             L"%H:%M:%S"
#define DB_TIME_FORMAT          L"%4d-%2d-%2d %2d:%2d:%2d"

#define TICK_MIN				(60)
#define TICK_HOUR				(TICK_MIN * 60)
#define TICK_DAY				(TICK_HOUR * 24)

#define TICK_TO_MIN(x)			(x / TICK_MIN)
#define MIN_TO_TICK(x)			(x * TICK_MIN)

#define TICK_TO_HOUR(x)			(x / TICK_HOUR)
#define HOUR_TO_TICK(x)			(x * TICK_HOUR)

#define TICK_TO_DAY(x)			(x / TICK_DAY)
#define DAY_TO_TICK(x)			(x * TICK_DAY)

#define CLOCK					Clocks::getInstance()
#define NOW_TICK				CLOCK->systemTick()
#define NOW_STRING				CLOCK->nowTime()
#define NOW_TIMEPOINT			CLOCK->nowTimePoint()

class Clocks : public Singleton<Clocks>
{
private:
	tick_t	_startTick;

public:
	Clocks();
	~Clocks();

	tick_t	serverStartTick();
	tick_t	systemTick();
	tick_t	strToTick(tstring str, WCHAR *fmt = DB_TIME_FORMAT);

	tstring	nowTime(WCHAR *fmt = DATETIME_FORMAT);
	tstring	nowTimeWithMilliSec(WCHAR *fmt = DATETIME_FORMAT);
	tstring	tickToStr(tick_t tick, WCHAR *fmt = DATETIME_FORMAT);

	tstring today();
	tstring tomorrow();
	tstring yesterday();

	DayOfTheWeek todayOfTheWeek();

	timePoint nowTimePoint();
};
