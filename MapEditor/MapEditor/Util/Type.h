#pragma once
#include "..\stdafx.h"

// 타입 정의
typedef std::wstring tstring;
typedef std::time_t tick_t;
typedef std::chrono::system_clock::time_point timePoint;

#define LOADMAXSTRING 100
#define WIN_WIDTH 1280
#define WIN_HEIGHT 800

// 크기 정의
#define SIZE_8		8
#define SIZE_16		16
#define SIZE_32		32
#define SIZE_64		64
#define SIZE_128	128
#define SIZE_256	256
#define SIZE_1024	1024
#define SIZE_2048	2048
#define SIZE_4096	4096
#define SIZE_8192	8192

// 마우스 상태
typedef enum _mousestate{
	U,
	D,
	M
}MOUSESTATE;