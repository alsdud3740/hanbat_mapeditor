#include "..\stdafx.h"
#include "Clock.h"

Clocks::Clocks()
{
	this->_startTick = this->systemTick();
}

Clocks::~Clocks()
{
}

tick_t Clocks::strToTick(tstring str, WCHAR *fmt)
{
	int	year = 0;
	int	month = 0;
	int	day = 0;
	int	hour = 0;
	int	minute = 0;
	int	second = 0;

	swscanf_s(str.c_str(), fmt, &year, &month, &day, &hour, &minute, &second);

	tm time = { second, minute, hour, day, month - 1, year - 1900 };

	return mktime(&time);
}

tick_t Clocks::serverStartTick()
{
	return _startTick;
}

tick_t Clocks::systemTick()
{
	return system_clock::to_time_t(system_clock::now());
}

tstring Clocks::tickToStr(tick_t tick, WCHAR *fmt)
{
	std::array<WCHAR, SIZE_128> timeStr;

	tm time;
	localtime_s(&time, &tick);
	wcsftime(timeStr.data(), timeStr.size(), fmt, &time);

	return timeStr.data();
}

tstring Clocks::nowTime(WCHAR *fmt)
{
	return this->tickToStr(this->systemTick(), fmt);
}

tstring Clocks::nowTimeWithMilliSec(WCHAR *fmt)
{
	high_resolution_clock::time_point point = high_resolution_clock::now();
	milliseconds ms = duration_cast<milliseconds>(point.time_since_epoch());

	seconds s = duration_cast<seconds>(ms);
	tick_t t = s.count();
	std::size_t fractionalSeconds = ms.count() % 1000;
	std::array<WCHAR, SIZE_8> milliStr;
	snwprintf(milliStr, L"%03d", (int)(fractionalSeconds));
	tstring timeString = this->tickToStr(this->systemTick(), fmt);
	timeString += L".";
	timeString += milliStr.data();
	return timeString;
}

tstring Clocks::today()
{
	return this->tickToStr(this->systemTick(), DATE_FORMAT);
}

tstring Clocks::tomorrow()
{
	return this->tickToStr(this->systemTick() + DAY_TO_TICK(1), DATE_FORMAT);
}

tstring Clocks::yesterday()
{
	return this->tickToStr(this->systemTick() - DAY_TO_TICK(1), DATE_FORMAT);
}

DayOfTheWeek Clocks::todayOfTheWeek()
{
	tm time;
	tick_t tick = this->systemTick();
	localtime_s(&time, &tick);
	return (DayOfTheWeek)time.tm_wday;
}

timePoint Clocks::nowTimePoint()
{
	return system_clock::now();
}