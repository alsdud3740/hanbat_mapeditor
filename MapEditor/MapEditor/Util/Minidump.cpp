#include "..\stdafx.h"
#include "Minidumb.h"

extern HWND ghWnd;

MiniDump::MiniDump()
{
	::SetUnhandledExceptionFilter(execptionFilter);
}

LONG WINAPI MiniDump::execptionFilter(struct _EXCEPTION_POINTERS *exceptionInfo)
{
	_CrtMemDumpAllObjectsSince(NULL);

	HMODULE dumpDll = nullptr;
	dumpDll = ::LoadLibraryA("DBGHELP.DLL");
	if (!dumpDll) 
		return 0;
	
	tstring dumpPatch;
	dumpPatch += CLOCK->nowTime(L"D%Y-%m-%dT%H-%M-%S");
	dumpPatch += L".dmp";

	HANDLE file = ::CreateFile(dumpPatch.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file == INVALID_HANDLE_VALUE)
		return 0;

	_MINIDUMP_EXCEPTION_INFORMATION info;
	info.ThreadId = ::GetCurrentThreadId();
	info.ExceptionPointers = exceptionInfo;
	info.ClientPointers = NULL;

	WRITEDUMP dumpFunc = (WRITEDUMP)::GetProcAddress(dumpDll, "MiniDumpWriteDump");

	if (dumpFunc(GetCurrentProcess(), GetCurrentProcessId(), file, MiniDumpNormal, &info, NULL, NULL) == FALSE) {
		return 0;
	}

	::CloseHandle(file);

	return EXCEPTION_CONTINUE_SEARCH;
}

static MiniDump minidump;