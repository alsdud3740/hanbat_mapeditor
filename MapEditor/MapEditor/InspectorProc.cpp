#include "stdafx.h"
#include "System.h"

extern HWND ChildHwnd[2];

VOID SetEdit(Object* obj)
{
	SetDlgItemInt(ChildHwnd[1], IDC_IPOSX, obj->transform.x, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_IPOSY, obj->transform.y, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_IPOSZ, obj->transform.z, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_ISX, obj->scale.x, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_ISY, obj->scale.y, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_ISZ, obj->scale.z, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_IRX, obj->rotate.x, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_IRY, obj->rotate.y, TRUE);
	SetDlgItemInt(ChildHwnd[1], IDC_IRZ, obj->rotate.z, TRUE);

	SetFocus(GetDlgItem(ChildHwnd[1], IDC_IPOSX));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_IPOSY));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_IPOSZ));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_ISX));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_ISY));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_ISZ));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_IRX));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_IRY));
	SetFocus(GetDlgItem(ChildHwnd[1], IDC_IRZ));

	SetFocus(ChildHwnd[0]);
}

BOOL CALLBACK Inspector(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_INITDIALOG:
		SetDlgItemInt(hDlg, IDC_IPOSX, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_IPOSY, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_IPOSZ, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_ISX, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_ISY, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_ISZ, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_IRX, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_IRY, 0, TRUE);
		SetDlgItemInt(hDlg, IDC_IRZ, 0, TRUE);
		printf("Inspector Created\n");
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
			// TODO:
			
		}
		break;
	}

	return FALSE;
}