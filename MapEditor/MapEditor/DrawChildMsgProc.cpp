#include "stdafx.h"
#include "System.h"

// ��������
UINT Mouse_X = 0;
UINT Mouse_Y = 0;
extern UINT index;

VOID GetIndexObject(UINT index)
{
	printf("GetIndexObject = %d\n", index);

	Object* find = OBJECTMANAGER->FindObjWithIndex(index);

	if (find != nullptr)
	{
		printf("%f %f %f\n", find->transform.x, find->transform.y, find->transform.z);
		SetEdit(find);
	}
}

LRESULT WINAPI ChildMsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CREATE:
		printf("DrawChild Created\n");
		InitApp(hWnd);
		break;
	case WM_DESTROY:
		PAINTSTRUCT ps;
		PostQuitMessage(0);
		EndPaint(hWnd, &ps);
		break;
	case WM_PAINT:
		Update(hWnd);
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam), wParam);
		break;
	case WM_RBUTTONDOWN:
		RButtonInput(MOUSESTATE::D, LOWORD(lParam), HIWORD(lParam), wParam);
		break;
	case WM_LBUTTONDOWN:
		GetIndexObject(index);
		break;
	case WM_MOUSEMOVE:
		RButtonInput(MOUSESTATE::M, LOWORD(lParam), HIWORD(lParam), wParam);
		Mouse_X = LOWORD(lParam);
		Mouse_Y = HIWORD(lParam);
		break;
	case WM_RBUTTONUP:
		RButtonInput(MOUSESTATE::U, LOWORD(lParam), HIWORD(lParam), wParam);
		break;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}